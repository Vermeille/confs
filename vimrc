"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                Bootstrap                                "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


" disable for pathogen
filetype plugin off

" call pathogen
call pathogen#infect()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Text configs                               "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Backspaces
set backspace=2

" vi no compatibility
set nocompatible

" Change tabs to spaces
set expandtab

" tab = 4 spaces
set softtabstop=4

" indent = 4
set shiftwidth=4

" autoscroll when near border
set scrolloff=10

" decide what to do with tab
set smarttab

" autoindent when no rules
set autoindent

" fix width to 80 col
set textwidth=79

" color the 80th column
set colorcolumn=80

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  View                                   "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

colorscheme desert256

" numbering
set relativenumber

" color syntax
syntax on

"show trailing spaces in red background
set listchars=trail:!,tab:>-
set list
highlight SpecialKey ctermbg=red guibg=red

" show mode in status bar
set showmode

" show command being typed
set showcmd

" show statusline
set laststatus=2

" coloration for doxygen
let g:load_doxygen_syntax=1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Features config                             "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" search while typing
set incsearch

" ignore case except if contains a upper case letter
set ignorecase
set smartcase

" replace global by default
set gdefault

set wildmenu

" updatetime for plugins
set updatetime=1000

" Allow mouse
set mouse=a

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  Keys                                   "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" leader is space
"let mapleader=" "

" Space Space goes to next buffer
noremap <space><space> <C-w>w

" Compile with F5
noremap <F5> :silent! :make \| :redraw! \| :botright :cw<cr>

" Open tagbar with F2
nnoremap <silent><F2> :TagbarToggle<CR>

" Open syntax errors whith F3
nnoremap <silent><F3> :Errors<CR>

" <Space>d for generate Doxygen
noremap <silent><space>d :Dox<CR>

" <Space>a for alternate file
noremap <silent><space>a :A<CR>

" <Space>A for alternate file in another buffer
noremap <silent><space>A :AV<CR>

" <Space>pv to pad variables
noremap <silent><space>pv :Tab / [a-zA-Z_*]*;<cr>

" <Space>pm to pad macros
noremap <silent><space>pm :Tab /\\/l1<cr>

" <Space>pp to pad prototypes
noremap <silent><space>pp :Tab /^[^ ]*\zs/<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                Variables                                "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:email = "guillaume.sanchez@epita.fr"
let g:login = "sanche_g"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Little Scripts                              "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

autocmd BufWritePre,FileWritePre * silent ks|call LastMod()|'s
fun LastMod()
    if line("$") > 20
        let l = 20
    else
        let l = line("$")
    endif
    exe "1," . l . "g/Last update /s/Last update .*/Last update " .
                \ strftime("%Y-%m-%d %H:%M") . " ". $USER
endfun

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Plugins                                 "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" activate plugins and indentation rules
filetype plugin indent on

set completeopt=menu,longest
setlocal omnifunc=syntaxcomplete#Complete

""""""""""""""""""""
"  clang_complete  "
""""""""""""""""""""

"let g:clang_periodic_quickfix=1
" Use libclang.so instead of the clang executable for clang_complete
"let g:clang_use_library=1

" Use snippets to complete
let g:clang_snippets=1
let g:clang_snippets_engine="ultisnips"

"""""""""""""""
"  syntastic  "
"""""""""""""""

" auto check on open
let g:syntastic_check_on_open=1

" Automatically open/close errors buffer
let g:syntastic_auto_loc_list=1

augroup templates
    autocmd!
    "autocmd BufNewFile * :Template %:e
augroup END

